# Jenkins for CollabraLink

The implementation of Jenkins for CollabraLink is built out here. It's a [Docker](https://docker.com)-based implementation that can be launched into AWS ECS. 

The Jenkins implementation integrates with Google Login to lock down the instance to only those individuals inside CollabraLink with Google credentials. This can be later modified to integrate with Active Directory.

This relies on the [Jenkins Configuration-as-Code Plugin](https://github.com/jenkinsci/configuration-as-code-plugin) for creating the settings for the Jenkins image.

In advance of running the Docker container, go to the [Google Developer's Console](https://console.developers.google.com) and follow the directions from the Google Login Jenkins Plugin [README](https://github.com/jenkinsci/google-login-plugin/blob/master/README.md) to get an OAuth credential that can be used for this project.

Make sure to set the Authorized redirect URIs appropriately in the Google Console. For developer machines, most likely the redirect URI will be `http://localhost:8080/securityRealm/finishLogin` or something like it.

Storing the client ID and client secret can vary - using Docker secrets or environment variables can work, though Vault would be better, as described

## Launching

### Launching into AWS ECS

*To-do*

### Launching on a local server

*To-do*

### Launching on a developer machine

Only do this to develop against the Jenkins implementation itself. 

The [docker](/docker) directory contains everything to start up the Jenkins server, including configuration. 

Create a `.env ` file which contains the environment variables `GOOGLE_OAUTH_CLIENT_ID` and `GOOGLE_OAUTH_CLIENT_SECRET` set to the appropriate information from the Google console when the OAuth 2.0 client ID was created.

Build the container with

```
$ docker build -t <tag> --rm .
```

Run the image locally with 

```
$ docker run -it -p 50000:50000 -p 8080:8080 <tag>
```

The mapping of port 50000 is to make the JNLP port visible. The port 8080 mapping opens up the Jenkins server on local host. After running the above, you will be able to log into the Jenkins environment at `http://localhost:8080`.